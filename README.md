# Edusoft

São 2 projetos:

   * EdusoftReact
   * EdufostRest


## Instalação 

É preciso ter instalado o [Node](https://nodejs.org/) para rodar o projeto do front.

```bash
cd edusoftreact
npm install
```

## Uso

```bash
npm run dev
```
