package br.com.edusoft.resources;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.lowagie.text.pdf.PdfObject;

import br.com.edusoft.domain.Aluno;
import br.com.edusoft.domain.DomainResult;
import br.com.edusoft.domain.Notas;
import br.com.edusoft.domain.ResultadoAluno;
import br.com.edusoft.domain.ResultadoAlunoDomainResult;
import br.com.edusoft.operations.Frequencia;
import br.com.edusoft.operations.Media;
import br.com.edusoft.operations.interfaces.Contas;
import br.com.edusoft.reports.AlunosReportDataAssembler;
import br.com.edusoft.reports.AlunosReportInput;
import br.com.edusoft.reports.ReportLocation;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;

@RestController
@RequestMapping(value = "/")
public class AlunosResource {

	private static final String URL_WEBSERVICE = "http://desenvolvimento.edusoft.com.br/desenvolvimentoMentorWebG5/rest/servicoexterno";
	private Date dtRequest;
	private String token;
	String usuario;
	String senha;
	List<ResultadoAluno> listResultado = new ArrayList<>();
	
	List<Aluno> listAlunos = new ArrayList<>();
	
	RestTemplate restTemplate = new RestTemplate();
	ResultadoAlunoDomainResult resultAlunoDomainResult = new ResultadoAlunoDomainResult();

	@RequestMapping(value = "/alunos", method = RequestMethod.GET)
	@CrossOrigin(origins = "http://localhost:3000")
	public ResponseEntity<DomainResult[]> listAll(HttpServletRequest httpServletRequest) {

		try {
			StringBuffer link = httpServletRequest.getRequestURL();

			usuario = httpServletRequest.getParameter("username");
			senha = httpServletRequest.getParameter("password");

			Map<String, String> params = new HashMap<>();
			params.put("username", usuario);
			params.put("password", senha);

			RestTemplate restTemplate = new RestTemplate();

			ResponseEntity<String> response = restTemplate.exchange(link.toString() + "/token?username={username}&password={password}", HttpMethod.GET, null,
					String.class, params);

			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Type", "application/json");
			headers.add("token", response.getBody());

			HttpEntity entity = new HttpEntity(headers);

			ResponseEntity<DomainResult[]> responsePost = restTemplate.exchange(URL_WEBSERVICE + "/execute/recuperaAlunos", HttpMethod.POST, entity,
					DomainResult[].class);

			DomainResult[] domain = responsePost.getBody();
			List<ResultadoAluno> listResultado = new ArrayList<>();
			
			for (Aluno aluno : domain[0].getAlunos()) {
				ResultadoAluno resultadoAluno = new ResultadoAluno();

				Contas contas = new Contas();
				OptionalDouble notas = aluno.getNotas().stream().mapToDouble(Notas::getNota).reduce((a, b) -> a + b);
				OptionalInt faltas = aluno.getNotas().stream().mapToInt(Notas::getFaltas).reduce((a, b) -> a + b);

				double media = contas.mostraCalculo(new Media(), notas.getAsDouble(), aluno.getNotas().size());
				double frequencia = contas.mostraCalculo(new Frequencia(), faltas.getAsInt(), aluno.getToalAulas());
				String resultado = frequencia < 70 ? "RF" : media >= 7 ? "AP" : "RM";
				resultadoAluno.setCodAluno(aluno.getCod());
				resultadoAluno.setMedia(round(media, 2));
				resultadoAluno.setResultado(frequencia < 70 ? "RF" : media >= 7 ? "AP" : "RM");
				resultadoAluno.setSeuNome("Anderson Marques de Freitas");
				listResultado.add(resultadoAluno);
				
				aluno.setAdditionalProperty("media", round(media, 2));
				aluno.setAdditionalProperty("frequencia", round(frequencia, 2));
				aluno.setAdditionalProperty("resultado", resultado);
				listAlunos.add(aluno);
			}
			
			resultAlunoDomainResult.setResultadoAluno(listResultado);

			return ResponseEntity.ok().body(domain);

		} catch (Exception ex) {
			Logger.getLogger(AlunosResource.class.getName()).log(Level.SEVERE, null, ex);
			return ResponseEntity.status(HttpStatus.OK).body(null);
		}

	}

	@RequestMapping(value = "/alunos/token", method = RequestMethod.GET)
	public ResponseEntity<String> getToken(@RequestParam String username, @RequestParam String password) {
		try {

			HttpHeaders headers = new HttpHeaders();
			headers.add("usuario", username);
			headers.add("senha", password);

			HttpEntity entity = new HttpEntity(headers);

			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.exchange(URL_WEBSERVICE + "/token/recuperaAlunos", HttpMethod.GET, entity, String.class);
			token = response.getBody();
			dtRequest = new Date();// salvo a data e hora que foi feita a request

			// para fins de teste
//			Random random = new Random();
//			
//			token = String.valueOf(random.nextInt());

			return ResponseEntity.ok().body(token);
		} catch (Exception ex) {
			Logger.getLogger(AlunosResource.class.getName()).log(Level.SEVERE, null, ex);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error");
		}

	}

	@RequestMapping(value = "/alunos/sendResult", method = RequestMethod.GET)
	@CrossOrigin(origins = "http://localhost:3000")
	public ResponseEntity<String> sendResult() {
		try {

			HttpHeaders headers = new HttpHeaders();
			headers.add("usuario", usuario);
			headers.add("senha", senha);

			HttpEntity entity = new HttpEntity(headers);

			ResponseEntity<String> response = restTemplate.exchange(URL_WEBSERVICE + "/token/gravaResultado", HttpMethod.GET, entity, String.class);

			headers = new HttpHeaders();
			headers.add("Content-Type", "application/json");
			headers.add("token", response.getBody());
//			entity = new HttpEntity(headers);
			HttpEntity<ResultadoAlunoDomainResult> request = new HttpEntity<>(resultAlunoDomainResult, headers);
			URI uri = new URI(URL_WEBSERVICE + "/execute/gravaResultado");

			// ResponseEntity<String> responsePost = restTemplate.exchange(URL_WEBSERVICE +
			// "/execute/gravaResultado", HttpMethod.POST, entity, String.class,
			// resultAlunoDomainResult);

			ResponseEntity<String> responsePost = restTemplate.postForEntity(uri, request, String.class);

			return ResponseEntity.ok().body(responsePost.getBody());
		} catch (Exception ex) {
			Logger.getLogger(AlunosResource.class.getName()).log(Level.SEVERE, null, ex);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error");
		}
	}

	@RequestMapping(value = "/alunos/report", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@CrossOrigin(origins = "http://localhost:3000")
	public ResponseEntity<byte[]> report() {
		ResponseEntity<byte[]> reportMounted = null;
		AlunosReportInput studentReportInput = AlunosReportDataAssembler.assemble(listAlunos);

		byte[] reportData = null;
		try {
//			JRMapArrayDataSource dataSource = new JRMapArrayDataSource(new Object[] { studentReportInput.getDataSources() });
			JRBeanCollectionDataSource dataSource = studentReportInput.getStudentDataSource();
			
			String reportSource = ReportLocation.alunoReportTemplate();

			JasperPrint jasperPrint = JasperFillManager.fillReport(reportSource, studentReportInput.getParameters(), dataSource);

			reportData = JasperExportManager.exportReportToPdf(jasperPrint);

			HttpHeaders respHeaders = new HttpHeaders();
			respHeaders.setContentLength(reportData.length);
			respHeaders.setContentType(MediaType.APPLICATION_PDF);
			respHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			respHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=report.pdf");
			
			
			reportMounted = new ResponseEntity<byte[]>(reportData, respHeaders, HttpStatus.OK);

		} catch (JRException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reportMounted;
	}

	private static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		BigDecimal bd = new BigDecimal(Double.toString(value));
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
}
