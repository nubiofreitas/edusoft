package br.com.edusoft.reports;

import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class AlunosReportInput {

	private String reportTitle;
	private String instituicaoNome;
	private JRBeanCollectionDataSource alunosDataSource;

	public String getInstituicaoNome() {
		return instituicaoNome;
	}

	public void setInstituicaoNome(String instituteName) {
		this.instituicaoNome = instituteName;
	}

	public JRBeanCollectionDataSource getStudentDataSource() {
		return alunosDataSource;
	}

	public void setStudentDataSource(JRBeanCollectionDataSource alunosDataSource) {
		this.alunosDataSource = alunosDataSource;
	}

	public Map<String, Object> getParameters() {
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("P_INSTITUTUICAO_NAME", getInstituicaoNome());
		parameters.put("P_REPORT_TITULO", getReportTitle());
		return parameters;
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}
}