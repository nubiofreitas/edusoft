package br.com.edusoft.reports;

import java.util.Collection;
import java.util.List;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class AlunosReportDataAssembler {
	   public static AlunosReportInput assemble(List<?> list) {
		   AlunosReportInput alunosReportInput = new AlunosReportInput();
		   alunosReportInput.setReportTitle("Relatório Simples Alunos");
		   alunosReportInput.setInstituicaoNome("Edusoft");
	 
	       
	       JRBeanCollectionDataSource alunosDataSource = new JRBeanCollectionDataSource(list, false);
	       alunosReportInput.setStudentDataSource(alunosDataSource);
	 
	       return alunosReportInput;
	   }
	}