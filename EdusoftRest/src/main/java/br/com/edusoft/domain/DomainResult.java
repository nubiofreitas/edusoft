package br.com.edusoft.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "resultado", "alunos" })
public class DomainResult implements Serializable {

	@JsonProperty("resultado")
	private String resultado;
	@JsonProperty("alunos")
	private List<Aluno> alunos = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("resultado")
	public String getResultado() {
		return resultado;
	}

	@JsonProperty("resultado")
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	@JsonProperty("alunos")
	public List<Aluno> getAlunos() {
		return alunos;
	}

	@JsonProperty("alunos")
	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}