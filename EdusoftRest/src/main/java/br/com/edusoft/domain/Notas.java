package br.com.edusoft.domain;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "NOTA", "FALTAS" })
public class Notas {

	@JsonProperty("NOTA")
	private Double nota;
	@JsonProperty("FALTAS")
	private Integer faltas;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("NOTA")
	public Double getNota() {
		return nota;
	}

	@JsonProperty("NOTA")
	public void setNota(Double nOTA) {
		this.nota = nOTA;
	}

	@JsonProperty("FALTAS")
	public Integer getFaltas() {
		return faltas;
	}

	@JsonProperty("FALTAS")
	public void setFaltas(Integer fALTAS) {
		this.faltas = fALTAS;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}