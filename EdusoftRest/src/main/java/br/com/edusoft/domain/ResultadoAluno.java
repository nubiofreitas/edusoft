package br.com.edusoft.domain;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "MEDIA", "RESULTADO", "COD_ALUNO", "SEU_NOME" })
public class ResultadoAluno {

	@JsonProperty("MEDIA")
	private Double media;
	@JsonProperty("RESULTADO")
	private String resultado;
	@JsonProperty("COD_ALUNO")
	private Integer codAluno;
	@JsonProperty("SEU_NOME")
	private String seuNome;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("MEDIA")
	public Double getMedia() {
		return media;
	}

	@JsonProperty("MEDIA")
	public void setMedia(Double mEDIA) {
		this.media = mEDIA;
	}

	@JsonProperty("RESULTADO")
	public String getResultado() {
		return resultado;
	}

	@JsonProperty("RESULTADO")
	public void setResultado(String rESULTADO) {
		this.resultado = rESULTADO;
	}

	@JsonProperty("COD_ALUNO")
	public Integer getCodAluno() {
		return codAluno;
	}

	@JsonProperty("COD_ALUNO")
	public void setCodAluno(Integer cODALUNO) {
		this.codAluno = cODALUNO;
	}

	@JsonProperty("SEU_NOME")
	public String getSeuNome() {
		return seuNome;
	}

	@JsonProperty("SEU_NOME")
	public void setSeuNome(String sEUNOME) {
		this.seuNome = sEUNOME;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}