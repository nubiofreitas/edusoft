package br.com.edusoft.domain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "COD", "NOME", "TOAL_AULAS", "nota" })
public class Aluno {

	@JsonProperty("COD")
	private Integer cod;
	@JsonProperty("NOME")
	private String nome;
	@JsonProperty("TOAL_AULAS")
	private Integer toalAulas;
	@JsonProperty("nota")
	private List<Notas> notas = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("COD")
	public Integer getCod() {
		return cod;
	}

	@JsonProperty("COD")
	public void setCod(Integer cOD) {
		this.cod = cOD;
	}

	@JsonProperty("NOME")
	public String getNome() {
		return nome;
	}

	@JsonProperty("NOME")
	public void setNome(String nOME) {
		this.nome = nOME;
	}

	@JsonProperty("TOAL_AULAS")
	public Integer getToalAulas() {
		return toalAulas;
	}

	@JsonProperty("TOAL_AULAS")
	public void setToalAulas(Integer tOALAULAS) {
		this.toalAulas = tOALAULAS;
	}

	@JsonProperty("nota")
	public List<Notas> getNotas() {
		return notas;
	}

	@JsonProperty("nota")
	public void setNotas(List<Notas> nota) {
		this.notas = nota;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
