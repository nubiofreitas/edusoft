package br.com.edusoft.operations.interfaces;

public interface Operacoes {
	public Double calcular(double val1, double val2);
}
