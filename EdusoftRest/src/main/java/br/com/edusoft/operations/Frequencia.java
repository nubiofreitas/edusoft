package br.com.edusoft.operations;

import br.com.edusoft.operations.interfaces.Operacoes;

public class Frequencia implements Operacoes {

	@Override
	public Double calcular(double val1, double val2) {

		return (100 - (100 * val1) / val2);
	}

}
