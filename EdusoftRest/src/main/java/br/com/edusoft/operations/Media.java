package br.com.edusoft.operations;

import br.com.edusoft.operations.interfaces.Operacoes;

public class Media implements Operacoes{

	@Override
	public Double calcular(double val1, double val2) {
		
		return val1 / val2;
	}

}
