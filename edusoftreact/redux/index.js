import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducer from "./reducers";

/**
 * Redux-thunk para dar suporte a funções assincronas
 * Basicamente vai ser usado sempre, principalmente nas requisições
 * pois retornam promises
 * @param {*} initialState
 */
export const initStore = (initialState = {}) => {
	return createStore(reducer, initialState, applyMiddleware(thunk));
};




/**
 * Aqui é a Store 
 * É o container que armazena e centraliza o estado geral da aplicação. 
 * Ela é imutável, ou seja, nunca se altera, apenas evolui
 * Esse é o diferencial do redux, centralizar a troca de estado da aplicação, e só pode ter uma store por aplicação
 * Como uma única fonte da verdade.
 * 
 * Vai facilitar muito o gerenciamento de estados da aplicação
 * Ele a grosso modo vem resolver problemas do estado sendo alterado em lugares que você nem sabe onde.
 */