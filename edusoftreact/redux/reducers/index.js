import { combineReducers } from "redux";
import loginReducer from "./logInReducer";

/**
 * Aqui poderiamos ter vários reducers aninhados, podendo criar vários reducers para dar
 * tratamentos a diferentes informações dentro da aplicação, facilitando a manutenção e o desacoplamento,
 * além de possibilitar incorporar várias funcionalidades no futuro
 *
 */
const rootReducer = combineReducers({
	authentication: loginReducer
	//email: emailReducer
	//user: userReducer, etc
});

export default rootReducer;
