import { LOGIN, SEND_RESULT, GET_REPORT } from "../types";

const initialState = {
	alunos: []
};

/**
 *  LoginReducer
 *  Recebe e trata as informações que serão ou não enviadas a store
 */

export default (state = initialState, action) => {
	switch (action.type) {
		case LOGIN:
			return {
				...state,
				// alunos: state.alunos.concat(action.payload.alunos),
				// result: action.payload.resultado
				//Para testar ws
				alunos: action.payload
			};
		case SEND_RESULT:
			return { ...state };
		case GET_REPORT:
			return { ...state };
		default:
			return state;
	}
};
