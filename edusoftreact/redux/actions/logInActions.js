import { performRequest } from "../../performRequest";
import Router from "next/router";
import { LOGIN, SEND_RESULT, GET_REPORT } from "../types";

const getAuth = formProps => async dispatch => {
	try {
		console.info("formProps", formProps);

		//axios retorna uma promise
		const response = await performRequest("GET", "/alunos", formProps);

		//Caso obtenha resposta do servidor passa o type da action e o dados para o reducer,
		//ele que vai manipular e tratar essas informações
		dispatch({
			type: LOGIN,
			payload: response.data[0]
		});

		Router.push("/alunos");
	} catch (e) {
		console.log("erro: ", e.response);
	}
};

const sendResult = () => async dispatch => {
	try {
		const response = await performRequest("GET", "/alunos/sendResult", null);
		console.info("response", response.data);
		dispatch({
			type: SEND_RESULT,
			payload: response.data
		});
	} catch (e) {
		console.log("erro", e.response);
	}
};

const getReport = () => async dispatch => {
	try {
		const response = await performRequest("GET", "/alunos/report", null);
		console.info('response', response);

		dispatch({
			type: GET_REPORT,
			payload: "SUCCESS"
		});
		

		return response;

		//Router.push(pdfAsDataUri);
	} catch (e) {
		console.log(e.response);
	}
};


export default {
	getAuth,
	sendResult,
	getReport
};
