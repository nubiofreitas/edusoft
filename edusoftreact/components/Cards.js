import React from "react";
import {
	Container,
	Grid,
	Card,
	CardMedia,
	CardContent,
	Typography,
	CardActions,
	Button,
	CardHeader,
	Avatar,
	IconButton
} from "@material-ui/core";
import MoreVertIcon from "@material-ui/icons/MoreVert";

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
	icon: {
		marginRight: theme.spacing(2)
	},
	heroContent: {
		backgroundColor: theme.palette.background.paper,
		padding: theme.spacing(8, 0, 6)
	},
	heroButtons: {
		marginTop: theme.spacing(4)
	},
	cardGrid: {
		paddingTop: theme.spacing(8),
		paddingBottom: theme.spacing(8)
	},
	card: {
		height: "100%",
		display: "flex",
		flexDirection: "column"
	},
	cardMedia: {
		paddingTop: "56.25%" // 16:9
	},
	cardContent: {
		flexGrow: 1
	},
	footer: {
		backgroundColor: theme.palette.background.paper,
		padding: theme.spacing(6)
	}
}));

const cards = [1, 2, 3];

export default function Mycards(props) {
	const classes = useStyles();

	const handleClick = () => {
        alert("click")
    };

	return (
		<main>
			<Container className={classes.cardGrid} maxWidth="md">
				{/* End hero unit */}
				<Grid container spacing={4}>
					{props.alunos.alunos.map(row => {
						let soma = row.nota.reduce((prevVal, elem) => {
							return prevVal + elem.NOTA;
						}, 0);
						let media = soma / row.nota.length;

						let faltas = row.nota.reduce((prevVal, elem) => {
							return prevVal + elem.FALTAS;
						}, 0);

						let frequencia = 100 - (100 * faltas) / row.TOAL_AULAS;

						return (
							<Grid item key={row.COD} xs={12} sm={6} md={4}>
								<Card className={classes.card}>
									<CardHeader title={row.NOME} />

									<CardContent className={classes.cardContent}>
										<Typography gutterBottom variant="h5" component="h2">
											Média
										</Typography>
										<Typography>{media.toFixed(2)}</Typography>
										<Typography gutterBottom variant="h5" component="h2">
											Frequência
										</Typography>
										<Typography>{frequencia.toFixed(2)}</Typography>
                                        <Typography gutterBottom variant="h5" component="h2">
											Resultado
										</Typography>
										<Typography>{frequencia < 70 ? "RF" : media >7 ? "AP" : "RM"}</Typography>
									</CardContent>
								</Card>
							</Grid>
						);
					})}
				</Grid>
			</Container>
		</main>
	);
}
