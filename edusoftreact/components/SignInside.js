import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import LocalLibrary from "@material-ui/icons/LocalLibrary";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import Link from "@material-ui/core/Link";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import actions from "../redux/actions";
import { connect } from "react-redux";
import { compose } from "redux";

function Copyright() {
	return (
		<Typography variant="body2" color="textSecondary" align="center">
			{"Copyright © "}
			{new Date().getFullYear()}
			{". Feito Com "}
			<Link color="inherit" href="https://material-ui.com/">
				Material-UI.
			</Link>
		</Typography>
	);
}

const useStyles = theme => ({
	root: {
		height: "100vh"
	},
	image: {
		backgroundImage:
			"url(https://images.unsplash.com/photo-1519682337058-a94d519337bc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80)",
		backgroundRepeat: "no-repeat",
		backgroundSize: "cover",
		backgroundPosition: "center"
	},
	paper: {
		margin: theme.spacing(8, 4),
		display: "flex",
		flexDirection: "column",
		alignItems: "center"
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main
	},
	form: {
		width: "100%", // Fix IE 11 issue.
		marginTop: theme.spacing(1)
	},
	submit: {
		margin: theme.spacing(3, 0, 2)
	}
});

class SignInSide extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			values: {
				username: "mentor",
				password: "123456",
				showPassword: false
			}
		};
	}

	handleClickShowPassword = () => {
		// console.log("click!");
		this.setState({
			values: {
				...this.state.values,
				showPassword: !this.state.values.showPassword
			}
		});
	};

	handleMouseDownPassword = event => {
		event.preventDefault();
	};

	handleSubmit = event => {
		event.preventDefault();
		console.log("click");
		this.props.getAuth({
			username: this.state.values.username,
			password: this.state.values.password
		});
	};

	render() {
		const { classes } = this.props;

		return (
			<Grid container component="main" className={classes.root}>
				<CssBaseline />
				<Grid item xs={false} sm={4} md={7} className={classes.image} />
				<Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
					<div className={classes.paper}>
						<Avatar className={classes.avatar}>
							<LocalLibrary />
						</Avatar>
						<Typography component="h1" variant="h5">
							Edusoft
						</Typography>
						<form
							className={classes.form}
							onSubmit={this.handleSubmit}
							noValidate
						>
							<TextField
								disabled
								variant="outlined"
								margin="normal"
								value={this.state.values.username}
								required
								fullWidth
								id="email"
								label="Login"
								name="email"
								autoComplete="email"
								autoFocus
							/>
							<TextField
								disabled
								variant="outlined"
								margin="normal"
								required
								fullWidth
								name="password"
								label="Senha"
								type={this.state.values.showPassword ? "text" : "password"}
								value={this.state.values.password}
								id="password"
								autoComplete="current-password"
								InputProps={{
									endAdornment: (
										<InputAdornment position="end">
											<IconButton
												edge="end"
												aria-label="toggle password visibility"
												onClick={this.handleClickShowPassword}
												onMouseDown={this.handleMouseDownPassword}
											>
												{this.state.values.showPassword ? (
													<Visibility />
												) : (
													<VisibilityOff />
												)}
											</IconButton>
										</InputAdornment>
									)
								}}
							/>
							<FormControlLabel
								control={<Checkbox value="remember" color="primary" />}
								label="Relembrar"
							/>
							<Button
								type="submit"
								fullWidth
								variant="contained"
								color="primary"
								className={classes.submit}
							>
								Log In
							</Button>
							<Box mt={5}>
								<Copyright />
							</Box>
						</form>
					</div>
				</Grid>
			</Grid>
		);
	}
}

const mapStateToProps = state => {
	return {
		alunos: state.authentication.alunos
	};
};

export default compose(
	connect(
		mapStateToProps,
		actions
	),
	withStyles(useStyles)
)(SignInSide);
