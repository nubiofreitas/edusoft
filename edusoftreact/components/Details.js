import React from "react";
import Link from "@material-ui/core/Link";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Container from "@material-ui/core/Container";
import Title from "./Title";

// Generate Order Data
function createData(id, date, name, shipTo, paymentMethod, amount) {
	return { id, date, name, shipTo, paymentMethod, amount };
}

const rows = [
	createData(
		0,
		"16 Mar, 2019",
		"Elvis Presley",
		"Tupelo, MS",
		"VISA ⠀•••• 3719",
		312.44
	),
	createData(
		1,
		"16 Mar, 2019",
		"Paul McCartney",
		"London, UK",
		"VISA ⠀•••• 2574",
		866.99
	),
	createData(
		2,
		"16 Mar, 2019",
		"Tom Scholz",
		"Boston, MA",
		"MC ⠀•••• 1253",
		100.81
	),
	createData(
		3,
		"16 Mar, 2019",
		"Michael Jackson",
		"Gary, IN",
		"AMEX ⠀•••• 2000",
		654.39
	),
	createData(
		4,
		"15 Mar, 2019",
		"Bruce Springsteen",
		"Long Branch, NJ",
		"VISA ⠀•••• 5919",
		212.79
	)
];

const useStyles = makeStyles(theme => ({
	seeMore: {
		marginTop: theme.spacing(3)
	}
}));

export default function Details(props) {
	const classes = useStyles();
	console.log("alunos", props.alunos);

	return (
		<React.Fragment>
			<Container className={classes.cardGrid} maxWidth="md">
				<Title>Alunos</Title>
				<Table size="small">
					<TableHead>
						<TableRow>
							<TableCell>Cod</TableCell>
							<TableCell>Nome</TableCell>
							<TableCell>Total Aulas</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{/* {rows.map(row => (
							<TableRow key={row.id}>
								<TableCell>{row.date}</TableCell>
								<TableCell>{row.name}</TableCell>
								<TableCell>{row.shipTo}</TableCell>
							</TableRow>
            ))} */}
            {props.alunos.alunos.map(row => (
                <TableRow key={row.COD}>
								<TableCell>{row.COD}</TableCell>
                <TableCell>{row.NOME}</TableCell>
								<TableCell>{row.TOAL_AULAS}</TableCell>                
							</TableRow>
              ))}
					</TableBody>
				</Table>
			</Container>
		</React.Fragment>
	);
}
