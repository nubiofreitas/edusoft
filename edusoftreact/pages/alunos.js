import React from "react";
import { withStyles } from "@material-ui/core/styles";

import actions from "../redux/actions";
import { connect } from "react-redux";
import { compose } from "redux";
import Header from "../components/Header";
import Details from "../components/Details";
import MyCards from "../components/Cards";
import { Paper, Grid, Button, ButtonGroup } from "@material-ui/core";

const useStyles = theme => ({
	root: {},
	content: {
		// paddingTop: theme.spacing(4),
		paddingBottom: theme.spacing(4)
	},
	appBarSpacer: {
		paddinTop: "10px"
	},
	margin: {
		margin: theme.spacing(1)
	}
});

class Alunos extends React.Component {
	handleSubmitResults = () => {
		this.props.sendResult();
	};

	handleReport = () => {
		fetch('http://localhost:8081/alunos/report').then(response => {
			response.blob().then(blob => {
				let url = window.URL.createObjectURL(blob);
				let a = document.createElement("a");
				a.href = url;
				a.download = "report.pdf";
				a.click();
			});
		});
	};

	render() {
		const { classes } = this.props;

		return (
			<div className={classes.root}>
				<Header />

				<div className={classes.content}>
					<Grid container>
						<Grid item xs={12}>
							<Paper className={classes.paper}>
								<Details alunos={this.props.alunos} />
							</Paper>
						</Grid>
						<Grid item xs={12}>
							<Paper>
								<MyCards alunos={this.props.alunos} />
								<ButtonGroup
									fullWidth
									aria-label="full width outlined button group"
								>
									<Button
										variant="contained"
										color="primary"
										className={classes.margin}
										onClick={this.handleSubmitResults}
									>
										Enviar Resultados
									</Button>
									<Button
										variant="contained"
										color="primary"
										onClick={this.handleReport}
										formTarget={"_blank"}
										className={classes.margin}
									>
										Relatório
									</Button>
								</ButtonGroup>
							</Paper>
						</Grid>
					</Grid>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		alunos: state.authentication.alunos
	};
};

export default compose(
	connect(
		mapStateToProps,
		actions
	),
	withStyles(useStyles)
)(Alunos);
