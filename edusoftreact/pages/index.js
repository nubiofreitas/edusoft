import React from "react";
import dynamic from "next/dynamic";

const SignInWithNoSSR = dynamic(() => import("../components/SignInside"), {
	ssr: false
});

const Home = () => (
	<div>
		<SignInWithNoSSR />
	</div>
);

export default Home;
