import React from "react";
import { Provider } from "react-redux";
import App from "next/app";
import withRedux from "next-redux-wrapper";
import { initStore } from "../redux";
import { loadProgressBar } from "axios-progress-bar";
import "axios-progress-bar/dist/nprogress.css";

export default withRedux(initStore, { debug: true })(
	class MyApp extends App {
		static async getInitialProps({ Component, ctx }) {
			return {
				pageProps: {
					...(Component.getInitialProps
						? await Component.getInitialProps(ctx)
						: {})
				}
			};
		}

		componentDidMount() {
			loadProgressBar();
		}

		render() {
			const { Component, pageProps, store } = this.props;
			return (
				<Provider store={store}>

						<Component {...pageProps} />

				</Provider>
			);
		}
	}
);
