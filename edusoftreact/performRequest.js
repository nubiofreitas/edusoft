import axios from "axios";

/**
 *	Vou centralizar o request nesse cara aqui
 * @param {*} method  GET ou Post
 * @param {*} url  URL do recurso
 * @param {*} params Parametros GET/Params - POST/data
 * @param {*} auth  Só ou usar se usar JWT Token
 */
export const performRequest = (method, url, params, auth) => {
	const body = method === "GET" ? "params" : "data";

	const config = {
		method,
		url,
		baseURL: "http://localhost:8081",
		[body]: params || {}
	};
	return axios.request(config);
};
